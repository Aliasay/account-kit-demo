<?php
use Galiasay\AccountKit\AccountKit;
use Galiasay\AccountKit\Config;
use Galiasay\AccountKit\Client;

require(__DIR__ . '/vendor/autoload.php');

if (isset($_POST["code"])) {
    $client = new AccountKit(new Config([
        'app_id' => '257186571426759',
        'secret' => '0ff7f1b147d202a2f9275c98b0b506f5',
        'version' => 'v1.1'
    ]), new Client());

    try {
        $accessToken = $client->getAccessToken($_POST['code']);

        $data = $client->getData($accessToken);

    } catch (\Exception $e) {
        $error = $e->getMessage();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Account kit</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="jumbotron text-center" style="margin-top: 50px">
        <h1>Facebook Account Kit Demo</h1>

        <div class="row" style="margin-bottom: 20px">
            <div class="col-md-12">
                <button class="btn btn-info" onclick="smsLogin();">Login with SMS</button>
                <button class="btn btn-info" onclick="emailLogin();">Login with Email</button>
            </div>
        </div>

        <?php if (isset($data)) { ?>
        <div class="alert alert-success" style="word-break: break-all;">
            Response: <?php echo json_encode($data) ?>
        </div>
        <?php } else if (isset($error)) { ?>
        <div class="alert alert-danger">
            <?php echo $error ?>
        </div>
        <?php } ?>

        <form action="" method="POST" id="form">
            <input type="hidden" name="code" id="code">
            <input type="hidden" name="csrf_nonce" id="csrf_nonce">
        </form>
    </div>
</div>


<script src="https://sdk.accountkit.com/en_US/sdk.js"></script>

<script>
    // initialize Account Kit with CSRF protection
    AccountKit_OnInteractive = function(){
        AccountKit.init(
            {
                appId:"257186571426759",
                state:"asd",
                version:"v1.1",
                fbAppEventsEnabled:true
            }
        );
    };

    // login callback
    function loginCallback(response) {
        console.log(response);
        if (response.status === "PARTIALLY_AUTHENTICATED") {
            document.getElementById("code").value = response.code;
            document.getElementById("csrf_nonce").value = response.state;
            document.getElementById("form").submit();
        }
        else if (response.status === "NOT_AUTHENTICATED") {
            console.log("Authentication failure");
        }
        else if (response.status === "BAD_PARAMS") {
            console.log("Bad parameters");
        }
    }

    function smsLogin() {
        AccountKit.login(
            'PHONE',
            {},
            loginCallback
        );
    }

    function emailLogin() {
        AccountKit.login(
            'EMAIL',
            {},
            loginCallback
        );
    }
</script>
</body>
</html>


