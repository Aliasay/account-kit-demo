<?php
namespace Galiasay\AccountKit\Exceptions;

/**
 * Class InvalidConfigException
 * @package Galiasay\AccountKit\Exceptions
 */
class InvalidConfigException extends \RuntimeException
{

}